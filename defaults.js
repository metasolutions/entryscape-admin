define([
  'entryscape-commons/defaults',
], (defaults) => {
  const ns = defaults.get('namespaces');
  ns.add('esterms', 'http://entryscape.com/terms/');
  ns.add('dcat', 'http://www.w3.org/ns/dcat#');
  return defaults;
});
