define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/on',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'entryscape-commons/defaults',
  'dojo/text!./CreateDialogTemplate.html',
  'config',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'i18n!nls/esadGroup',
], (declare, lang, domAttr, domStyle, on, _WidgetsInTemplateMixin, NLSMixin, defaults, template,
    config, TitleDialog, ListDialogMixin) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esadGroup'],
    nlsHeaderTitle: 'createGroupHeader',
    nlsFooterButtonLabel: 'createGroupButton',

    postCreate() {
      this.inherited(arguments);
      this.dialog.lockFooterButton();
      if (config.admin && config.admin.showGroupName === true) {
        domStyle.set(this.groupNameNode, 'display', '');
        let t;
        const groupnameSearch = lang.hitch(this, this.usernameSearch);
        on(this.groupnameInput, 'keyup', () => {
          if (t != null) {
            clearTimeout(t);
          }
          t = setTimeout(groupnameSearch, 300);
        });
      }

      on(this.fullnameInput, 'keyup', lang.hitch(this, this.checkValidInfoDelayed));
    },
    usernameSearch() {
      this.newGroupnameIsOk = true;
      if (config.admin && config.admin.showGroupName === true) {
        // TODO check allowed chars
        const groupname = domAttr.get(this.groupnameInput, 'value');
        if (groupname === '') {
          this.newGroupnameIsOk = true;
          domStyle.set(this.groupnameError, 'display', 'none');
          this.checkNewName();
          return;
        }
        this.newGroupnameIsOk = false;

        const es = defaults.get('entrystore');
        es.getREST().get(`${es.getBaseURI()}_principals?entryname=${groupname}`)
          .then(lang.hitch(this, (data) => {
            if (data.length > 0) {
              this.newGroupnameIsOk = false;
              domStyle.set(this.groupnameError, 'display', '');
              domAttr.set(this.groupnameError, 'innerHTML', this.NLSBundles.esadGroup.groupnameTaken);
              // domClass.add(this.createGroupButton, "disabled");
              this.dialog.lockFooterButton();
            } else {
              throw Error('No matching group.');
            }
          })).then(null, lang.hitch(this, () => {
            this.newGroupnameIsOk = true;
            domStyle.set(this.groupnameError, 'display', 'none');
            this.checkValidInfoDelayed();
          }));
      }
    },
    open() {
      this.list.getView().clearSearch();
      domAttr.set(this.groupnameInput, 'value', '');
      domAttr.set(this.fullnameInput, 'value', '');
      this.dialog.lockFooterButton();
      this.dialog.show();
    },

    checkValidInfoDelayed() {
      if (this.delayedCheckTimout != null) {
        clearTimeout(this.delayedCheckTimout);
      }

      this.delayedCheckTimeout = setTimeout(lang.hitch(this, this.checkNewName), 300);
    },

    checkNewName() {
      if (config.admin && config.admin.showGroupName === true) {
        const fullname = domAttr.get(this.fullnameInput, 'value');
        if (fullname === '' || this.newGroupnameIsOk === false) {
          this.dialog.lockFooterButton();
        } else {
          this.dialog.unlockFooterButton();
        }
      } else {
        const fullname = domAttr.get(this.fullnameInput, 'value');
        if (fullname === '' || this.newGroupnameIsOk === false) {
          this.dialog.lockFooterButton();
        } else {
          this.dialog.unlockFooterButton();
        }
      }
    },
    footerButtonAction() {
      const groupname = domAttr.get(this.groupnameInput, 'value');
      const fullname = domAttr.get(this.fullnameInput, 'value');
      const createContext = domAttr.get(this.createContextWithGroup, 'checked');
      if (config.admin && config.admin.showGroupName === true) {
        if (fullname === '' || this.newGroupnameIsOk === false) {
          return false;
        }
      } else if (fullname === '' || this.newGroupnameIsOk === false) {
        return false;
      }
      /** @type {store/EntryStore} */
      const store = defaults.get('entrystore');
      const ns = defaults.get('namespaces');
      const dialogs = defaults.get('dialogs');
      let groupEntry;
      let contextEntry;

      let pue;
      if (config.admin && config.admin.setGroupNameAsEntryIdOnCreate && defaults.get('isAdmin')) {
        pue = store.newGroup(groupname, groupname);
      } else {
        pue = store.newGroup(groupname);
      }
      const md = pue.getMetadata();
      md.add(pue.getResourceURI(), ns.expand('foaf:name'),
        { type: 'literal', value: fullname });
      return pue.commit().then((ue) => {
        groupEntry = ue;
        if (createContext) {
          let cpe;
          if (groupname && config.admin.setContextNameAsEntryIdOnCreate) {
            cpe = store.newContext(groupname, groupname);
          } else {
            cpe = store.newContext();
          }
          cpe.getEntryInfo().setACL({ admin: [groupEntry.getResourceURI()] });
          return cpe.commit();
        }
        return groupEntry;
      }).then((ce) => {
        if (createContext) {
          contextEntry = ce;
          return groupEntry.getResource(true).setHomeContext(contextEntry.getId()).then(() => {
            contextEntry.setRefreshNeeded();
            return contextEntry.refresh();
          });
        }
        return ce;
      }).then(lang.hitch(this, () => {
        // Everything worked!
        this.list.getView().addRowForEntry(groupEntry);
      }), lang.hitch(this, (err) => {
        // Something did not work, try to clean up.
        if (groupEntry != null) {
          groupEntry.del();
        }
        if (contextEntry != null) {
          contextEntry.del();
        }
        dialogs.acknowledge(err);
      }));
    },
  }));
