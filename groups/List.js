define([
  'dojo/_base/declare',
  'store/types',
  './CreateDialog',
  'entryscape-commons/defaults',
  'config',
  'entryscape-admin/utils/ACLDialog',
  'entryscape-admin/utils/NameDialog',
  './GroupRow',
  './MemberDialog',
  'entryscape-commons/list/common/BaseList',
  'i18n!nls/escoList',
  'i18n!nls/esadGroup',
], (declare, types, CreateDialog, defaults, config, ACLDialog, NameDialog, GroupRow,
    MemberDialog, BaseList) => {
  const GroupACLDialog = declare([ACLDialog], {
    nlsBundles: ['esadGroup'],
    nlsHeaderTitle: 'groupACLHeader',
    nlsFooterButtonLabel: 'updateGroupACLButton',
  });

  const GroupNameDialog = declare([NameDialog], {
    nlsBundles: ['esadGroup'],
    lookUpPath: '_principals?entryname=',
  });

  return declare([BaseList], {
    nlsBundles: ['escoList', 'esadGroup'],
    includeInfoButton: false,
    rowClickDialog: 'edit',
    rowClass: GroupRow,
    rowActionNames: ['edit', 'versions', 'share', 'members', 'remove'],

    postCreate() {
      this.registerDialog('members', MemberDialog);
      this.registerDialog('share', GroupACLDialog);
      if (config.admin && config.admin.showGroupName === true) {
        this.registerDialog('name', GroupNameDialog);
        this.registerRowAction({
          name: 'name',
          button: 'default',
          icon: 'tag',
          iconType: 'fa',
          nlsKey: 'groupName',
        });
        this.rowActionNames.splice(2, 0, 'name');
      }
      this.registerRowAction({
        first: true,
        name: 'members',
        button: 'default',
        icon: 'users',
        iconType: 'fa',
        nlsKey: 'memberList',
      });
      this.registerRowAction({
        name: 'share',
        button: 'default',
        icon: 'key',
        iconType: 'fa',
        nlsKey: 'groupACL',
      });
      this.inherited('postCreate', arguments);
      this.registerDialog('create', CreateDialog);
      this.dialogs.edit.levels.setIncludeLevel('recommended');
    },

    showStopSign() {
      return !defaults.get('hasAdminRights');
    },

    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem('esc:Group');
      }
      return this.template;
    },
    getSearchObject() {
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().entryType(types.ET_LOCAL)
        .graphType(types.GT_GROUP)
        .resourceType('InformationResource');// types.RT_INFORMATIONRESOURCE);
    },
    canShowView() {
      return new Promise(resolve => defaults.get('hasAdminRights', resolve));
    },
  });
});
