module.exports = (grunt) => {
  grunt.task.loadTasks('node_modules/entryscape-js/tasks');

  grunt.config.merge({
    poeditor: {
      projectids: ['94877'],
    },
    nls: {
      langs: ['en', 'sv', 'de'],
      depRepositories: ['entryscape-commons'],
    },
    update: {
      libs: [
        'di18n',
        'spa',
        'rdfjson',
        'rdforms',
        'store',
        'entryscape-commons',
      ],
    },
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-available-tasks');
};
