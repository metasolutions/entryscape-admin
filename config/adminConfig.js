define({
  admin: {
    showContextName: false,
    showGroupName: false,
    showContextTypeControl: false,
    setGroupNameAsEntryIdOnCreate: false,
    setContextNameAsEntryIdOnCreate: false,
  },
  itemstore: {
    bundles: [
      'templates/dcterms/dcterms',
      'templates/foaf/foaf',
      'templates/vcard/vcard',
      'templates/entryscape/esc',
    ],
  },
  site: {
    modules: {
      admin: {
        productName: 'Admin',
        faClass: 'cogs',
        restrictTo: 'admin',
        startView: 'admin__users',
        sidebar: true,
      },
    },
    views: {
      admin: {
        class: 'entryscape-commons/nav/Cards',
        title: { en: 'Administration', sv: 'Administration', de: 'Administration' },
        route: '/admin',
        module: 'admin',
        sidebar: false,
      },
      admin__users: {
        class: 'entryscape-admin/users/List',
        faClass: 'user',
        title: { en: 'Users', sv: 'Användare', de: 'Benutzer' },
        route: '/admin/users',
        parent: 'admin',
        module: 'admin',
      },
      admin__groups: {
        class: 'entryscape-admin/groups/List',
        faClass: 'users',
        title: { en: 'Groups', sv: 'Grupper', de: 'Gruppen' },
        route: '/admin/groups',
        parent: 'admin',
        module: 'admin',
      },
      admin__projects: {
        class: 'entryscape-admin/contexts/List',
        faClass: 'building',
        title: { en: 'Projects', sv: 'Projekt', de: 'Projekte' },
        route: '/admin/projects',
        parent: 'admin',
        module: 'admin',
      },
    },
  },
  contexttypes: {
    catalogContext: {
      rdfType: 'http://entryscape.com/terms/CatalogContext',
      entryType: 'dcat:Catalog',
    },
    terminologyContext: {
      rdfType: 'http://entryscape.com/terms/TerminologyContext',
      entryType: 'skos:ConceptScheme',
    },
    workbenchContext: {
      rdfType: 'http://entryscape.com/terms/WorkbenchContext',
    },
    context: {
      rdfType: 'http://entryscape.com/terms/Context',
    },
  },
});
