define([
  'entryscape-commons/merge',
  'entryscape-admin/config/adminConfig',
], (merge, adminConfig) => merge({
  theme: {
    appName: 'EntryScape Admin',
    oneRowNavbar: true,
    localTheme: false,
  },
  locale: {
    fallback: 'en',
    supported: [
      { lang: 'de', flag: 'de', label: 'Deutsch', labelEn: 'German' },
      { lang: 'en', flag: 'gb', label: 'English', labelEn: 'English' },
      { lang: 'sv', flag: 'se', label: 'Svenska', labelEn: 'Swedish' },
    ],
  },
  site: {
    siteClass: 'spa/Site',
    controlClass: 'entryscape-commons/nav/Layout',
    startView: 'adminstart',
  },
}, adminConfig, __entryscape_config));
