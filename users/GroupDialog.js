define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/dom-class',
  'dojo/dom-construct',
  'di18n/NLSMixin',
  'entryscape-commons/list/common/ListDialogMixin',
  'entryscape-commons/list/common/TypeaheadList',
  'entryscape-commons/defaults',
  'store/types',
  './GroupList',
  'entryscape-commons/dialog/TitleDialog',
  'i18n!nls/esadUser',
], (declare, lang, array, domClass, domConstruct, NLSMixin, ListDialogMixin, TypeaheadList,
    defaults, types, GroupList, TitleDialog) => {
  const RemoveDialog = declare([], {
    constructor(params) {
      this.list = params.list;
    },
    open(params) {
      const self = this;
      const user = this.list.entry;
      const group = params.row.entry;
      // check user role in group
      const dialogs = defaults.get('dialogs');
      const grpEntryInfo = group.getEntryInfo();
      if (grpEntryInfo.getACL().admin.indexOf(user.getResourceURI()) !== -1) {
        dialogs.confirm(this.list.NLSBundles.esadGroup.removeGrpWithMgr,
          null, null, lang.hitch(this, (confirm) => {
            if (!confirm) {
              return;
            }
            // remove
            self.removeGroup(params);
          }));
      } else {
        self.removeGroup(params);
      }
    },
    removeGroup(params) {
      const self = this;
      const user = this.list.entry;
      const group = params.row.entry;
      group.getResource(true).removeEntry(user)
        .then(() => {
          user.setRefreshNeeded(true);
          user.refresh();
          self.list.getView().removeRow(params.row);
          self.list.groupList.refresh();
          params.row.destroy();
        });
    },
  });

  const GroupListView = declare([TypeaheadList], {
    nlsBundles: ['escoList', 'esadUser', 'esadGroup'],
    includeInfoButton: true,
    includeCreateButton: false,
    includeEditButton: false,
    includeRemoveButton: true,
    nlsEmptyListWarningKey: 'emptyMessageGroups',

    postCreate() {
      this.inherited('postCreate', arguments);
      this.registerDialog('remove', RemoveDialog); // Overrides the removeDialog from BaseList.
      domClass.remove(this.getView().domNode, 'container');
      this.listView.includeResultSize = !!this.includeResultSize; // make this boolean
    },

    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem('esc:Group');
      }
      return this.template;
    },

    typeahead_getQuery(str) {
      return this.entry.getEntryStore().newSolrQuery().graphType(types.GT_GROUP).title(str)
        .limit(10)
        .list();
    },

    typeahead_select(entry) {
      entry.getResource(true).addEntry(this.entry).then(lang.hitch(this, () => {
        this.entry.setRefreshNeeded();
        this.entry.refresh();
        this.groupList.refresh();
      }));
      this.inherited(arguments);
    },

    typeahead_processEntries(entries, callback) {
      const self = this;
      const groups = this.entry.getParentGroups();
      const filtEntries = array.filter(entries, e => groups.indexOf(e.getURI()) === -1);
      callback(array.map(filtEntries, entry => ({
        id: entry.getURI(),
        name: self.typeahead.getLabel(entry),
      })));
    },

    search(/* params */) {
      if (this.groupList == null || this.entry !== this.groupList.userEntry) {
        this.groupList = new GroupList(this.entry);
      }
      this.getView().showEntryList(this.groupList);
    },
  });

  return declare([TitleDialog, ListDialogMixin, NLSMixin.Dijit], {
    nlsBundles: ['esadUser'],
    nlsHeaderTitle: 'groupHeader',
    nlsFooterButtonLabel: 'groupDoneButton',
    maxWidth: 800,

    postCreate() {
      this.inherited(arguments);
      this.groupList = new GroupListView({}, domConstruct.create('div', null, this.containerNode));
    },

    open(params) {
      this.inherited(arguments);
      this.entry = params.row.entry;
      this.groupList.entry = params.row.entry;
      this.groupList.render();
      if (this.NLSBundle0) {
        this.updateLocaleStrings(this.NLSBundle0, {
          user: defaults.get('rdfutils').getLabel(this.entry) || this.entry.getId(),
        });
      }
      this.show();
    },
  });
});
