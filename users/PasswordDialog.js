define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/on',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'entryscape-commons/list/common/ListDialogMixin',
  'dojo/text!./PasswordDialogTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/auth/Password',
  'entryscape-commons/auth/components/PasswordForm',
  'i18n!nls/esadUser',
], (declare, lang, domAttr, domStyle, domClass, on, _WidgetsInTemplateMixin, NLSMixin,
    ListDialogMixin, template, TitleDialog, Password, PasswordForm) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esadUser'],
    nlsHeaderTitle: 'setPasswordHeader',
    nlsFooterButtonLabel: 'setPasswordButton',

    open(params) {
      this.inherited(arguments);
      Password.clear();
      m.mount(this.passwordFormNode, PasswordForm('admin', this.check.bind(this)));

      this.userEntry = params.row.entry;
      this.dialog.lockFooterButton();
      this.dialog.show();
    },
    check() {
      if (Password.canSubmit() && Password.provided()) {
        this.dialog.unlockFooterButton();
      } else {
        this.dialog.lockFooterButton();
      }
    },
    footerButtonAction() {
      this.userEntry.getResource().then(resourceEntry =>
        resourceEntry.setPassword(Password.password)
          .then(null, lang.hitch(this, () => {
            throw this.NLSBundles.esadUser.setPasswordError;
          })));
    },
  }));
