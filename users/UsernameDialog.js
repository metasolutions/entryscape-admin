define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/on',
  'entryscape-commons/defaults',
  'dojo/text!./UsernameDialogTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'i18n!nls/esadUser',
], (declare, lang, domAttr, domStyle, on, defaults, template, TitleDialog, ListDialogMixin) =>

  declare([TitleDialog.ContentNLS, ListDialogMixin], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esadUser'],
    popoverOptions: {},
    nlsHeaderTitle: 'changeUsernameHeader',
    nlsFooterButtonLabel: 'changeUsernameButton',

    postCreate() {
      this.inherited(arguments);

      let t;
      const usernameSearch = lang.hitch(this, this.usernameSearch);
      on(this.usernameInput, 'keyup', () => {
        if (t != null) {
          clearTimeout(t);
        }
        t = setTimeout(usernameSearch, 300);
      });
    },
    usernameSearch() {
      // TODO check allowed chars
      const username = domAttr.get(this.usernameInput, 'value');
      if (username === '' || username === this.currentUsername) {
        domStyle.set(this.usernameError, 'display', 'none');
        this.dialog.lockFooterButton();
        return;
      }
      this.newNameIsOk = false;

      const es = defaults.get('entrystore');
      es.getREST().get(`${es.getBaseURI()}_principals?entryname=${username}`)
        .then(lang.hitch(this, (data) => {
          if (data.length > 0) {
            this.newNameIsOk = false;
            domStyle.set(this.usernameError, 'display', '');
            domAttr.set(this.usernameError, 'innerHTML', this.NLSBundles.esadUser.usernameTaken);
            this.dialog.lockFooterButton();
          } else {
            throw Error('No matching user.');
          }
        })).then(null, lang.hitch(this, () => {
          this.newNameIsOk = true;
          domStyle.set(this.usernameError, 'display', 'none');
          this.dialog.unlockFooterButton();
        }));
    },
    open() {
      this.inherited(arguments);
      domAttr.set(this.usernameInput, 'value', '');
      this.dialog.lockFooterButton();
      this.dialog.show();
      this.row.entry.getResource().then(lang.hitch(this, (user) => {
        this.currentUsername = user.getName();
        domAttr.set(this.currentUserName, 'value', this.currentUsername);
      }));
    },

    footerButtonAction() {
      const username = domAttr.get(this.usernameInput, 'value');

      if (username === '' || this.newNameIsOk === false) {
        return false;
      }

      return this.row.entry.getResource()
        .then(user => user.setName(username))
        .then(lang.hitch(this.list, this.list.rowMetadataUpdated, this.row));
    },
  }));
