define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'entryscape-commons/list/common/ListDialogMixin',
  'dojo/text!./CustomPropertiesDialogTemplate.html',
  'dojo/dom-construct',
  'entryscape-commons/dialog/TitleDialog',
  'i18n!nls/esadUser',
], (declare, domAttr, _WidgetsInTemplateMixin, NLSMixin, ListDialogMixin, template, domConstruct,
    TitleDialog) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esadUser'],
    nlsHeaderTitle: 'customPropHeader',
    includeFooter: false,

    postCreate() {
      this.inherited(arguments);
    },
    open(params) {
      this.inherited(arguments);
      this.userEntry = params.row.entry;
      this._clearRows();
      this._renderRows();
    },
    _clearRows() {
      domAttr.set(this.customPropTableBody, 'innerHTML', '');
    },
    _renderRows() {
      const self = this;
      this.userEntry.getResource().then((resourceEntry) => {
        const customProperties = resourceEntry.getCustomProperties();
        Object.keys(customProperties).forEach((property) => {
          const value = customProperties[property];
          const tr = domConstruct.create('tr', null, self.customPropTableBody);
          domConstruct.create('td', { innerHTML: property }, tr);
          domConstruct.create('td', { innerHTML: value }, tr);
        });
        self.dialog.show();
      });
    },
  }));
