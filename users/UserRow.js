define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/string',
  'store/terms',
  'store/types',
  'entryscape-commons/defaults',
  'entryscape-commons/list/EntryRow',
  'entryscape-commons/dialog/ConfirmDialog',
], (declare, lang, string, terms, types, defaults, EntryRow, ConfirmDialog) => declare([EntryRow], {
  /**
   * @deprecated use corresponding method installActionOrNot.
   */
  installButtonOrNot() {
    const id = this.entry.getId();
    if (id === '_guest' || id === '_admin') {
      return 'disabled';
    }
    return this.inherited(arguments);
  },

  installActionOrNot(params) {
    const { name } = params;
    const id = this.entry.getId();
    if (id === '_guest' || id === '_admin') {
      switch (name) {
        case 'remove':
        case 'customProperties':
        case 'statusEnable':
        case 'statusDisable':
          return 'disabled';
        default:
          break;
      }
    }

    // don't install 'status' action if current user cannot administer
    // the row user or current = row
    if (name === 'statusEnable' || name === 'statusDisable') {
      const currentUserId = defaults.get('userEntry').getId();
      if (!this.entry.canAdministerEntry() || this.entry.getId() === currentUserId) {
        return false;
      }

      const isUserDisabled = this.getIsUserDisabled();
      if ((isUserDisabled && name === 'statusDisable') || (!isUserDisabled && name === 'statusEnable')) {
        return false;
      }
    }

    return this.inherited(arguments);
  },
  getRenderName() {
    const username = this.entry.getEntryInfo().getName() || (this.entry.getResource(true)
      && this.entry.getResource(true).getName());
    const rdfutils = defaults.get('rdfutils');
    const name = rdfutils.getLabel(this.entry);
    if (name == null && username == null) {
      return string.substitute(this.nlsSpecificBundle.unnamedUser, { id: this.entry.getId() });
    }

    const isUserDisabled = this.getIsUserDisabled();
    return `${username}&nbsp;&nbsp;-&nbsp;&nbsp;${name}${isUserDisabled ? this.getDisabledUserHtml() : ''}`;
  },
  action_remove() {
    const entry = this.entry;
    const es = entry.getEntryStore();
    const dialogs = defaults.get('dialogs');
    const bundle = this.nlsSpecificBundle;
    const name = this.getRenderName();
    let soleUserMsg = null;// fix for ESAD-5
    // Check if there are any solely owned non-homecontexts.
    es.newSolrQuery().graphType(types.GT_CONTEXT).admin(entry.getResourceURI()).limit(100)
      .getEntries(0)
      .then((ownedContextEntries = []) => {
        // Check if many solely owned entries.
        if (ownedContextEntries.some((oce) => {
            // If solely owned.
            if (oce.getEntryInfo().getACL().admin.length === 1) {
              // Ignore homecontexts since they will be checked later.
              const homeContextOf = oce.getReferrers(terms.homeContext);
              if (homeContextOf.indexOf(entry.getResourceURI()) === -1) {
                return true;
              }
            }
            return false; // Not solely owned or homecontext.
          })) {
          soleUserMsg = string.substitute(bundle.warningSoleOwner, { name });
        }
      })
      .then(() => {
        // fix for ESAD-5
        let msg = string.substitute(bundle.remove, { name });
        if (soleUserMsg) {
          msg = `${soleUserMsg}<br><br>${string.substitute(bundle.remove, { name })}`;
        }
        return dialogs.confirm(msg, bundle.confirmRemove, bundle.cancelRemove);
      })
      .then(lang.hitch(this, () =>  // Proceed to remove user
        // Third, check if we should remove solely owned homecontext as well.
        entry.getResource().then((ures) => {
          const hcId = ures.getHomeContext();
          if (hcId) {
            const hcontext = es.getContextById(hcId);
            const res = hcontext.getEntry();
            return res.then((hcEntry) => {
              const refs = hcEntry.getReferrers(terms.homeContext);
              if (refs.length === 1 && refs[0] === entry.getResourceURI()) {
                return dialogs.confirm(bundle.removeHomecontext,
                  bundle.confirmRemoveHomecontext,
                  bundle.cancelRemoveHomecontext)
                  .then(() => {
                    hcEntry.del();
                    return entry.del();
                  }, () => {
                    const d = entry.del();
                    hcEntry.setRefreshNeeded();
                    hcEntry.refresh();
                    return d;
                  });
              }
              return entry.del();
            });
          }
          return entry.del();
        }).then(lang.hitch(this, () => {
          this.list.getView().removeRow(this);
          this.destroy();
        }), (err) => {
          dialogs.acknowledge(string.substitute(bundle.failedRemove, { error: err || '' }));
        })));
  },
  action_statusEnable() {
    this.changeStatus();
  },
  action_statusDisable() {
    this.changeStatus();
  },
  changeStatus() {
    const isUserDisabled = this.getIsUserDisabled();
    const message = isUserDisabled ? this.nlsSpecificBundle.userStatusEnableConfirmation :
      this.nlsSpecificBundle.userStatusEnableConfirmation;
    const yes = this.nlsSpecificBundle.userStatusChangeConfirm;
    const no = this.nlsSpecificBundle.userStatusChangeCancel;

    new ConfirmDialog().show(message, yes, no, async (isConfirmed) => {
      if (isConfirmed) {
        // change user status
        const res = await this.entry.getResource();
        await res.setDisabled(!isUserDisabled);
        this.reRender();
      }
    });
  },
  getIsUserDisabled() {
    const currentUserId = defaults.get('userEntry').getId();
    if (this.entry.getId() === currentUserId) {
      return false;
    }

    return this.entry.getEntryInfo().isDisabled();
  },
  getDisabledUserHtml() {
    return `<i style="margin-left:10px"  rel="tooltip" title="${this.nlsSpecificBundle.userStatusDisabled}" class="fa fa-user-times"></i>`;
  },
}));
