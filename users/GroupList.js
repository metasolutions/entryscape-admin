define([
  'dojo/_base/declare',
  'dojo/_base/array',
  'dojo/promise/all',
  'dojo/Deferred',
  'dojo/_base/lang',
  'entryscape-commons/defaults',
], (declare, array, all, Deferred, lang, defaults) => declare(null, {
  limit: 20,

  constructor(userEntry) {
    this.userEntry = userEntry;
  },

  refresh() {
    delete this.groups;
  },

  getEntries(page) {
    const es = defaults.get('entrystore');
    if (this.groups == null) {
      const groupURIs = this.userEntry.getParentGroups();
      return all(array.map(groupURIs, guri => es.getEntry(guri)))
        .then(lang.hitch(this, (groups) => {
          this.groups = groups;
          return this.getEntries(page);
        }));
    }
    const d = new Deferred();
    d.resolve(this.groups.slice(page * this.getLimit(), (page + 1) * this.getLimit()));
    return d;
  },
  getLimit() {
    return this.limit;
  },
  getSize() {
    return this.groups == null ? -1 : this.groups.length;
  },
}));
