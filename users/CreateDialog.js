define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/on',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'entryscape-commons/defaults',
  'dojo/text!./CreateDialogTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'i18n!nls/esadUser',
], (declare, lang, domAttr, domStyle, on, _WidgetsInTemplateMixin, NLSMixin, defaults, template,
    TitleDialog, ListDialogMixin) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esadUser'],
    popoverOptions: {},
    nlsHeaderTitle: 'createUserHeader',
    nlsFooterButtonLabel: 'createUserButton',

    postCreate() {
      this.inherited(arguments);

      let t;
      const usernameSearch = lang.hitch(this, this.usernameSearch);
      on(this.usernameInput, 'keyup', () => {
        if (t != null) {
          clearTimeout(t);
        }
        t = setTimeout(usernameSearch, 300);
      });
      on(this.firstnameInput, 'keyup', lang.hitch(this, this.checkValidInfoDelayed));
      on(this.lastnameInput, 'keyup', lang.hitch(this, this.checkValidInfoDelayed));
    },
    usernameSearch() {
      // TODO check allowed chars
      const username = domAttr.get(this.usernameInput, 'value');
      if (username === '') {
        domStyle.set(this.usernameError, 'display', 'none');
        return;
      }
      this.newNameIsOk = false;

      const es = defaults.get('entrystore');
      es.getREST().get(`${es.getBaseURI()}_principals?entryname=${username}`)
        .then(lang.hitch(this, (data) => {
          if (data.length > 0) {
            this.newNameIsOk = false;
            domStyle.set(this.usernameError, 'display', '');
            domAttr.set(this.usernameError, 'innerHTML', this.NLSBundles.esadUser.usernameTaken);
            this.dialog.lockFooterButton();
          } else {
            throw Error('No matching user.');
          }
        })).then(null, lang.hitch(this, () => {
          this.newNameIsOk = true;
          domStyle.set(this.usernameError, 'display', 'none');
          this.checkValidInfoDelayed();
        }));
    },
    open() {
      this.list.getView().clearSearch();
      domAttr.set(this.usernameInput, 'value', '');
      domAttr.set(this.firstnameInput, 'value', '');
      domAttr.set(this.lastnameInput, 'value', '');
      this.dialog.lockFooterButton();
      this.dialog.show();
    },

    checkValidInfoDelayed() {
      if (this.delayedCheckTimeout != null) {
        clearTimeout(this.delayedCheckTimeout);
      }

      this.delayedCheckTimeout = setTimeout(lang.hitch(this, this.checkNewName), 300);
    },

    checkNewName() {
      const username = domAttr.get(this.usernameInput, 'value');
      const firstname = domAttr.get(this.firstnameInput, 'value');
      const lastname = domAttr.get(this.lastnameInput, 'value');
      if (username === '' || firstname === '' || lastname === '' || this.newNameIsOk === false) {
        this.dialog.lockFooterButton();
      } else {
        this.dialog.unlockFooterButton();
      }
    },
    footerButtonAction() {
      const username = domAttr.get(this.usernameInput, 'value');
      const firstname = domAttr.get(this.firstnameInput, 'value');
      const lastname = domAttr.get(this.lastnameInput, 'value');
      const createContext = domAttr.get(this.createContextWithUser, 'checked');

      if (username === '' || firstname === '' || lastname === '' || this.newNameIsOk === false) {
        return false;
      }
      /** @type {store/EntryStore} */
      const store = defaults.get('entrystore');
      const ns = defaults.get('namespaces');
      const dialogs = defaults.get('dialogs');
      let userEntry;
      let contextEntry;

      const pue = store.newUser(username);
      const md = pue.getMetadata();
      const resURI = pue.getResourceURI();
      const guestURI = store.getResourceURI('_principals', '_guest');
      md.add(pue.getResourceURI(), ns.expand('foaf:givenName'),
        { type: 'literal', value: firstname });
      md.add(pue.getResourceURI(), ns.expand('foaf:familyName'),
        { type: 'literal', value: lastname });
      md.add(pue.getResourceURI(), ns.expand('foaf:name'),
        { type: 'literal', value: `${firstname} ${lastname}` });
      pue.getEntryInfo().setACL({
        mread: [guestURI],
        rread: [guestURI],
        mwrite: [resURI],
        rwrite: [resURI],
      });

      return pue.commit().then((ue) => {
        userEntry = ue;
        if (createContext) {
          const cpe = store.newContext();
          cpe.getEntryInfo().setACL({ admin: [userEntry.getResourceURI()] });
          return cpe.commit();
        }
        return userEntry;
      }).then((ce) => {
        if (createContext) {
          contextEntry = ce;
          return userEntry.getResource(true).setHomeContext(contextEntry.getId()).then(() => {
            contextEntry.setRefreshNeeded();
            return contextEntry.refresh();
          });
        }
        return ce;
      }).then(lang.hitch(this, () => {
        // Everything worked!
        this.list.getView().addRowForEntry(userEntry);
      }), lang.hitch(this, (err) => {
        // Something did not work, try to clean up.
        if (userEntry != null) {
          userEntry.del();
        }
        if (contextEntry != null) {
          contextEntry.del();
        }

        dialogs.acknowledge(err);
      }));
    },
  }));
