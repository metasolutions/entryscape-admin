define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/on',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'entryscape-commons/defaults',
  'dojo/text!./CreateDialogTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'config',
  'i18n!nls/esadContext',
], (declare, lang, domAttr, domStyle, domClass, on, _WidgetsInTemplateMixin, NLSMixin, defaults,
    template, TitleDialog, ListDialogMixin, config) =>
  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esadContext'],
    nlsHeaderTitle: 'newContextnameHeader',
    nlsFooterButtonLabel: 'createContextButton',

    postCreate() {
      this.inherited(arguments);

      if (config.admin && config.admin.showContextName === true) {
        domStyle.set(this.contextNameNode, 'display', '');
        let t;
        const contextnameSearch = lang.hitch(this, this.contextNameSearch);
        on(this.contextnameInput, 'keyup', () => {
          if (t != null) {
            clearTimeout(t);
          }
          t = setTimeout(contextnameSearch, 300);
        });
      }

      on(this.fullnameInput, 'keyup', lang.hitch(this, this.checkValidInfoDelayed));
    },
    contextNameSearch() {
      // TODO check allowed chars
      this.newNameIsOk = true;

      if (config.admin && config.admin.showContextName === true) {
        const contextname = domAttr.get(this.contextnameInput, 'value');
        if (contextname === '') {
          this.newNameIsOk = true;
          domStyle.set(this.contextnameError, 'display', 'none');
          this.checkNewName();
          return;
        }
        this.newNameIsOk = false;
        const es = defaults.get('entrystore');
        es.getREST().get(`${es.getBaseURI()}_contexts?entryname=${contextname}`)
          .then(lang.hitch(this, (data) => {
            if (data.length > 0) {
              this.newNameIsOk = false;
              domStyle.set(this.contextnameError, 'display', '');
              domAttr.set(this.contextnameError, 'innerHTML', this.NLSBundle0.contextnameTaken);
              // domClass.add(this.createContextButton, "disabled");
              this.dialog.lockFooterButton();
            } else {
              throw Error('No matching context.');
            }
          })).then(null, lang.hitch(this, () => {
            this.newNameIsOk = true;
            domStyle.set(this.contextnameError, 'display', 'none');
            this.checkValidInfoDelayed();
          }));
      }
    },
    open() {
      this.list.getView().clearSearch();
      domAttr.set(this.contextnameInput, 'value', '');
      domAttr.set(this.fullnameInput, 'value', '');
      // domClass.add(this.createContextButton, "disabled");
      this.dialog.lockFooterButton();
      this.dialog.show();
    },

    checkValidInfoDelayed() {
      if (this.delayedCheckTimout != null) {
        clearTimeout(this.delayedCheckTimout);
      }

      this.delayedCheckTimeout = setTimeout(lang.hitch(this, this.checkNewName), 300);
    },

    checkNewName() {
      const fullname = domAttr.get(this.fullnameInput, 'value');
      if (config.admin && config.admin.showContextName === true) {
        if (fullname === '' || this.newNameIsOk === false) {
          this.dialog.lockFooterButton();
        } else {
          this.dialog.unlockFooterButton();
        }
      } else if (fullname === '' || this.newNameIsOk === false) {
        this.dialog.lockFooterButton();
      } else {
        this.dialog.unlockFooterButton();
      }
    },
    footerButtonAction() {
      const contextname = domAttr.get(this.contextnameInput, 'value');
      const fullname = domAttr.get(this.fullnameInput, 'value');
      // createContext = domAttr.get(this.createContextWithGroup, "checked");
      if (config.admin && config.admin.showContextName === true) {
        if (fullname === '' || this.newNameIsOk === false) {
          return false;
        }
      } else if (fullname === '' || this.newNameIsOk === false) {
        return false;
      }

      /** @type {store/EntryStore} */
      const store = defaults.get('entrystore');
      const dialogs = defaults.get('dialogs');
      let contextEntry;
      let newEntryId;
      if (contextname && config.admin.setContextNameAsEntryIdOnCreate && defaults.get('isAdmin')) {
        newEntryId = contextname;
      }
      const pContextEntry = store.newContext(contextname, newEntryId);
      const md = pContextEntry.getMetadata();
      md.addL(pContextEntry.getResourceURI(), 'dcterms:title', fullname);
      return pContextEntry.commit().then((ue) => {
        contextEntry = ue;
        return contextEntry;
      }).then(ce => ce).then(lang.hitch(this, () => {
        this.list.getView().addRowForEntry(contextEntry);
      }), lang.hitch(this, (err) => {
        if (contextEntry != null) {
          contextEntry.del();
        }
        dialogs.acknowledge(err);
      }));
    },
  }));
