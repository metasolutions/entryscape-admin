define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'di18n/NLSMixin',
  'dijit/_WidgetsInTemplateMixin',
  'entryscape-commons/list/common/ListDialogMixin',
  'entryscape-commons/acl/ACL', // In template
  'entryscape-commons/dialog/TitleDialog',
  'i18n!nls/esadContext',
], (declare, lang, NLSMixin, _WidgetsInTemplateMixin, ListDialogMixin, ACL, TitleDialog) =>
  declare([TitleDialog.Content, ListDialogMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: '<div><div data-dojo-attach-point="acl" data-dojo-type="entryscape-commons/acl/ACL"></div></div>',
    maxWidth: 800,
    nlsBundles: ['esadContext'],
    nlsHeaderTitle: 'contextACLHeader',
    nlsFooterButtonLabel: 'updateContextACLButton',

    postCreate() {
      this.inherited(arguments);
      this.acl.onChange = lang.hitch(this, () => {
        this.dialog.unlockFooterButton();
      });
    },

    open(params) {
      this.inherited(arguments);
      this.entry = params.row.entry;
      this.acl.showEntry(this.entry);
      this.localeChange();
      this.dialog.lockFooterButton();
      this.dialog.show();
    },

    localeChange() {
      if (this.NLSBundle0 && this.row != null) {
        const res = this.row.getRenderName();// fix for [object Promise]
        if (typeof res === 'string') {
          this.dialog.updateLocaleStrings(this.NLSBundle0,
            { name: res });
        } else if (typeof res === 'object' && typeof res.then === 'function') {
          res.then(lang.hitch(this, (name) => {
            this.dialog.updateLocaleStrings(this.NLSBundle0,
              { name });
          }));
        }
      }
    },

    footerButtonAction() {
      return this.acl.saveACL();
    },
  }));
