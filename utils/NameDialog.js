define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/string',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/on',
  'di18n/NLSMixin',
  'entryscape-commons/defaults',
  'entryscape-commons/dialog/TitleDialog',
  'dojo/text!./NameDialogTemplate.html',
], (declare, lang, string, domAttr, domStyle, domClass, on, NLSMixin, defaults, TitleDialog,
    template) =>
  declare([TitleDialog.Content, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: [], // override
    lookUpPath: 'cid?entryname=', // override with the cid needed.
    nlsHeaderTitle: 'nameHeader',
    nlsFooterButtonLabel: 'updateNameButton',

    postCreate() {
      this.inherited(arguments);
      let t;
      const nameSearch = lang.hitch(this, this.nameSearch);
      on(this.nameInput, 'keyup', () => {
        if (t != null) {
          clearTimeout(t);
        }
        t = setTimeout(nameSearch, 300);
      });
    },

    nameUpdateMessage(state, oldname, newname) {
      let message;
      let cls;
      const bundle = this.NLSBundle0;
      switch (state) {
        case 'hide':
          domStyle.set(this.nameMessage, 'display', 'none');
          return;
        case 'clear':
          message = bundle.nameReset;
          cls = 'alert-info';
          break;
        case 'taken':
          message = bundle.nameTaken;
          cls = 'alert-warning';
          break;
        case 'malformed':
          message = bundle.nameMalformed;
          cls = 'alert-warning';
          break;
        case 'rename':
          message = bundle.nameRename;
          cls = 'alert-info';
          break;
        default:
          break;
      }
      domAttr.set(this.nameMessage, 'innerHTML',
        string.substitute(message, { name: oldname, newname }));
      domClass.remove(this.nameMessage, 'alert-danger');
      domClass.remove(this.nameMessage, 'alert-warning');
      domClass.remove(this.nameMessage, 'alert-info');
      domClass.add(this.nameMessage, cls);
      domStyle.set(this.nameMessage, 'display', '');
    },

    nameSearch() {
      // TODO check allowed chars
      const oldname = this.entry.getResource(true).getName() || '';
      const newname = domAttr.get(this.nameInput, 'value');
      if (newname === oldname) {
        this.nameUpdateMessage('hide');
        this.checkNewName();
        return;
      }

      if (newname.length > 0 && (newname.toLowerCase() !== newname
        || newname.indexOf(' ') !== -1)) {
        this.nameUpdateMessage('malformed', oldname, newname);
        this.newNameIsOk = false;
        this.checkNewName();
        return;
      }

      if (newname === '') {
        this.newNameIsOk = true;
        if (oldname !== '') {
          this.nameUpdateMessage('clear', oldname, '');
        } else {
          this.nameUpdateMessage('hide');
        }
        this.checkNewName();
        return;
      }
      this.newNameIsOk = false;

      const es = defaults.get('entrystore');
      es.getREST().get(es.getBaseURI() + this.lookUpPath + newname)
        .then(lang.hitch(this, (data) => {
          if (data.length > 0) {
            this.newNameIsOk = false;
            this.nameUpdateMessage('taken', oldname, newname);
          } else {
            throw Error('No matching entry.');
          }
        }))
        .then(null, lang.hitch(this, () => {
          this.newNameIsOk = true;
          this.nameUpdateMessage('rename', oldname, newname);
          this.checkNewName();
        }));
    },

    localeChange() {
      this.dialog.updateLocaleStrings(this.NLSBundle0);
    },

    open(params) {
      this.row = params.row;
      this.entry = params.row.entry;
      const name = this.entry.getResource(true).getName() || '';
      domAttr.set(this.nameInput, 'value', name);
      this.nameUpdateMessage('hide');
      this.checkNewName();
      this.dialog.show();
    },

    isSameName() {
      const newname = domAttr.get(this.nameInput, 'value');
      const oldname = this.entry.getResource(true).getName();
      return newname === oldname || (newname === '' && oldname == null);
    },

    checkNewName() {
      if (this.isSameName() || !this.newNameIsOk) {
        this.dialog.lockFooterButton();
      } else {
        this.dialog.unlockFooterButton();
      }
    },

    footerButtonAction() {
      const newname = domAttr.get(this.nameInput, 'value');
      return this.entry.getResource(true).setName(newname)
        .then(lang.hitch(this, () => {
          this.row.render(); // Everything worked, update row!
        }));
    },
  }));
